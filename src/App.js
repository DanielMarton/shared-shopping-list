import './App.css';
import {BrowserRouter  as Router, Route, Routes } from 'react-router-dom'
import Home from './components/Home';
import AddList from './components/AddList';

function App() {
  return (
    <Router>
      <div className="App">
        <h1>Dan's Shared Shopping List</h1>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/add_new" element={<AddList />} />
        </Routes>
      </div>
    </Router>

  );
}

export default App;
