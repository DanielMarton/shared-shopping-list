import { addDoc, collection } from "firebase/firestore"
import { doc, deleteDoc } from "firebase/firestore"
import { db } from "./FirebaseConfig"

export async function addNewList(newList){
    try {
        await addDoc(collection(db, 'lists'), newList)
    } catch(error){
        console.log(error)
    }
}

export async function deleteList(id){
    try {
        await deleteDoc(doc(db, 'lists', id))
    } catch(error){
        console.log(error)
    }
}