import { initializeApp } from "firebase/app"
import { getFirestore } from "firebase/firestore";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
    apiKey: "AIzaSyAFBH7zZjnYmyXBoWDirdjD7dDV1Jm7a_Y",
    authDomain: "shared-shopping-list-22cff.firebaseapp.com",
    projectId: "shared-shopping-list-22cff",
    storageBucket: "shared-shopping-list-22cff.appspot.com",
    messagingSenderId: "148732199649",
    appId: "1:148732199649:web:6a1904a9ae19260ca22da0",
    measurementId: "G-LMZQKV432J"
};

const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export const db = getFirestore(app)