import React from 'react'
import { deleteList } from "../firebase/FirebaseUtils"

function ShoppingListItem(props) {
    return (
        <div className='flex-row-container'>
            <div className='inner-container'>
                <p className='vertically-centered'>{props.listName}</p>
            </div>
            <div className='inner-container'>
                <button className='vertically-centered'
                        onClick={() => {deleteList(props.id)}}>
                Delete</button>
            </div>

        </div>
    )
}
export default ShoppingListItem