import React from 'react'
import { Link } from 'react-router-dom'
import ShoppingLists from './ShoppingLists'

 function Home() {
  return (
    <div>
        <ShoppingLists />
        <div>
            <Link to="/add_new">Add</Link>
        </div>
    </div>
  )
}
export default Home
