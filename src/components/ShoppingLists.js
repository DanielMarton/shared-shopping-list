import { collection, onSnapshot } from 'firebase/firestore'
import React, { useEffect, useState, useRef } from 'react'
import { db } from "../firebase/FirebaseConfig"
import ShoppingListItem from './ShoppingListItem'

function ShoppingLists() {

    const [lists, setLists] = useState([])
    const subscription = useRef()

    useEffect(() => {
        subscription.current = onSnapshot(collection(db, 'lists'),
            (response) => {
                let tempLists = []
                response.forEach((doc) => {
                    let tempElement = doc.data()
                    tempElement.id = doc.id
                    tempLists.push(tempElement)
                })
                setLists(tempLists)
            })
    }, [])

    useEffect(() => {
        return () => {
            subscription.current()
        }
    }, [])

    const printLists = () => {
        return lists.map(
            listItem => <ShoppingListItem key={listItem.id}
                                         id={listItem.id}
                                         created={listItem.created.seconds}
                                         listName={listItem.listName} />
        )
    }

    return (
        <div>
            <h1>Active Shopping Lists:</h1>
            <div>{printLists()}</div>
        </div>
    )
}
export default ShoppingLists