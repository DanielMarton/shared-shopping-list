import { Timestamp } from 'firebase/firestore'
import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { addNewList } from '../firebase/FirebaseUtils'

function AddList() {
    const [listName, setListName] = useState("")
    const navigate = useNavigate()

    const handleChange = (event) => {
        return setListName(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        addNewList({
            listName: listName,
            created: Timestamp.now(),
            listItems: []
        })
        navigate("/")
    }

    return (
        <div>
            <h1>Add new shopping list</h1>
            <Link to="/">Back</Link>
            <form onSubmit={handleSubmit} >
                <input type="text" name="name" value={listName} onChange={handleChange} />
                <input type='submit' value="Add" />
            </form>
        </div>
    )
}
export default AddList
