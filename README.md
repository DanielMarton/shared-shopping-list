# Dan's Shared Shopping List

This app intends to showcase my practical knowledge of Firebase, React and other technologies. I focus on writing clean and concise code.

## Overview

- The app is going to facilitate the creation of shopping lists that can be accessed from different devices.
- Backend is going to be provided by Firebase
- On the main screen existing shopping lists are going to appear with an interface to create a new shopping list
- When opening a shopping list, items can be added, removed or ticked to show, they have been bought

## Technologies used

- React
- React Router
- React Hooks
- Firebase
- CSS with a mobile first approach